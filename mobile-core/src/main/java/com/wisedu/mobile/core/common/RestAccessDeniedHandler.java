package com.wisedu.mobile.core.common;

import com.wisedu.mobile.common.AjaxResult;
import com.wisedu.mobile.common.constant.HttpStatus;
import com.wisedu.mobile.common.utils.MessageUtils;
import com.wisedu.mobile.common.utils.ServletUtils;
import org.nutz.json.Json;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.web.access.AccessDeniedHandler;
import org.springframework.stereotype.Component;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.text.MessageFormat;

/**
 * @Author: hj
 * @Date: 2020/3/16 下午5:26
 */
@Component
public class RestAccessDeniedHandler implements AccessDeniedHandler {
    @Override
    public void handle(HttpServletRequest request, HttpServletResponse response, AccessDeniedException e) throws IOException, ServletException {
        //登陆状态下，权限不足执行该方法
        int code = HttpStatus.FORBIDDEN;
        String msg = MessageFormat.format(MessageUtils.message("no.access.permission"), request.getRequestURI());
        ServletUtils.renderString(response, Json.toJson(AjaxResult.error(code, msg)));
    }
}
