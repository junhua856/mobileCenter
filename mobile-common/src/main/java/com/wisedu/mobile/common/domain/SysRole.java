package com.wisedu.mobile.common.domain;


import com.wisedu.mobile.common.BaseEntity;
import org.nutz.dao.entity.annotation.*;
import org.nutz.plugins.validation.annotation.Validations;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;
import java.util.List;

/**
 * 角色表 sys_role
 * 
 * @author hj
 */
@Table("sys_role")
@Comment("角色表")
public class SysRole extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /**
     * 角色名称
     */
    @Column("role_name")
    @Comment("角色名称")
    @Validations(required = true, errorMsg = "角色名称不能为空")
    @ColDefine(type = ColType.VARCHAR, width = 50)
    private String roleName;

    /**
     * 角色权限
     */
    @Column("role_key")
    @Comment("角色权限")
    @Validations(required = true, errorMsg = "角色权限不能为空")
    @ColDefine(type = ColType.VARCHAR, width = 10)
    private String roleKey;


    /**
     * 数据范围（0：所有数据权限；3：自定义数据权限；1：本部门数据权限；2：本部门及以下数据权限）
     */
    @Column("data_scope")
    @Comment("数据范围 ")
    @ColDefine(type = ColType.VARCHAR, width = 10)
    private String dataScope;

    /**
     * 角色状态（0正常 1停用）
     */
    @Column("status")
    @Comment("角色状态（0正常 1停用） ")
    @ColDefine(type = ColType.VARCHAR, width = 10)
    private int status;


    private boolean checked;
    /** 菜单组 */
    private String[] menuIds;

    /**
     * 部门组（数据权限）
     */
    private String[] deptIds;

    @ManyMany(from = "role_id", relation = "sys_role_menu", to = "menu_id")
    protected List<SysMenu> menus;

    public boolean isChecked() {
        return checked;
    }

    public void setChecked(boolean checked) {
        this.checked = checked;
    }

    public String getRoleName() {
        return roleName;
    }

    public void setRoleName(String roleName) {
        this.roleName = roleName;
    }

    public String getRoleKey() {
        return roleKey;
    }

    public void setRoleKey(String roleKey) {
        this.roleKey = roleKey;
    }

    public String getDataScope() {
        return dataScope;
    }

    public void setDataScope(String dataScope) {
        this.dataScope = dataScope;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String[] getMenuIds() {
        return menuIds;
    }

    public void setMenuIds(String[] menuIds) {
        this.menuIds = menuIds;
    }

    public String[] getDeptIds() {
        return deptIds;
    }

    public void setDeptIds(String[] deptIds) {
        this.deptIds = deptIds;
    }

    public List<SysMenu> getMenus() {
        return menus;
    }

    public void setMenus(List<SysMenu> menus) {
        this.menus = menus;
    }
}
