package com.wisedu.mobile.core.ratelimit;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;


@Configuration
public class RedisLimitWebMvcConfigurer implements WebMvcConfigurer {
    @Autowired
    private RateLimitCheckInterceptor rateLimitCheckInterceptor;

    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        registry.addInterceptor(rateLimitCheckInterceptor).addPathPatterns("/**");
    }

    @Bean
    public RedisRateLimiterFactory RedisRateLimiterFactory(){
        return  new RedisRateLimiterFactory();
    }
}
