let tableIns;
let tree;
layui.use(['element', 'form', 'table', 'layer', 'laydate','tree', 'util'], function () {
    let table = layui.table;
    let form = layui.form;//select、单选、复选等依赖form
    let element = layui.element; //导航的hover效果、二级菜单等功能，需要依赖element模块
    let laydate = layui.laydate;
    tree = layui.tree;
    let height = document.documentElement.clientHeight - 160;

    tableIns = table.render({
        elem: '#authorityTable'
        , url: ctx + '/manage/sys/role/page'
        , method: 'POST'
        //请求前参数处理
        , request: {
            pageName: 'pageNumber' //页码的参数名称，默认：page
            , limitName: 'pageSize' //每页数据量的参数名，默认：limit
        }
        , response: {
            statusName: 'code' //规定数据状态的字段名称，默认：code
            , statusCode: 200 //规定成功的状态码，默认：0
            , msgName: 'msg' //规定状态信息的字段名称，默认：msg
        }
        //响应后数据处理
        ,parseData: function (res) { //res 即为原始返回的数据
            return {
                "code": res.code, //解析接口状态
                "msg": res.msg, //解析提示文本
                "count": res.data.total, //解析数据长度
                "data": res.data.rows //解析数据列表
            };
        }
        , toolbar: '#authorityTableToolbarDemo'
        , title: '用户列表'
        , cols: [[
            {field: 'id', title: 'ID', hide: true}
            , {field: 'roleName', title: '权限名称'}
            , {field: 'roleKey', title: '权限key'}
            , {field: 'dataScope', title: '数据范围'}
            , {fixed: 'right', title: '操作', toolbar: '#authorityTableBarDemo'}
        ]]
        , defaultToolbar: ['', '', '']
        , page: true
        , height: height
        , cellMinWidth: 80
    });

    //头工具栏事件
    table.on('toolbar(test)', function (obj) {
        switch (obj.event) {
            case 'addData':
                //重置操作表单
                $("#authorityForm").show();
                $("#menuForm").hide();
                $("#authorityForm")[0].reset();
                form.render();
                layer.msg("请填写右边的表单并保存！");
                break;
            case 'query':
                let queryByAuthorityName = $("#queryByAuthorityName").val();
                let query = {
                    page: {
                        curr: 1 //重新从第 1 页开始
                    }
                    , done: function (res, curr, count) {
                        //完成后重置where，解决下一次请求携带旧数据
                        this.where = {};
                    }
                };
                if (queryByAuthorityName) {
                    //设定异步数据接口的额外参数
                    query.where = {roleName: queryByAuthorityName};
                }
                tableIns.reload(query);
                $("#queryByAuthorityName").val(queryByAuthorityName);
                break;
        }
    });

    //监听行工具事件
    table.on('tool(test)', function (obj) {
        let data = obj.data;
        //删除
        if (obj.event === 'del') {
            layer.confirm('确认删除吗？', function (index) {
                //向服务端发送删除指令
                $.delete(ctx + "/manage/sys/role/delete/" + data.id, {}, function (data) {
                    obj.del();
                    layer.close(index);
                })
            });
        }
        //编辑
        else if (obj.event === 'edit') {
            $("#authorityForm").show();
            $("#menuForm").hide();
            //回显操作表单
            $("#authorityForm").form(data);
            form.render();
        }
        //授权
        else if (obj.event === 'auth') {
            $("#authorityForm").hide();
            $("#menuForm").show();
            $("#roleid").val(data.id);
            loadAuthorityTree(data.id);
        }
    });
});

/**
 * 提交保存
 */
function authorityFormSave() {
    let authorityForm = $("#authorityForm").serializeObject();
    $.post(ctx + "/manage/sys/role/save", authorityForm, function (data) {
        layer.msg("保存成功", {icon: 1,time: 2000}, function () {});
        tableIns.reload();
    });
}
function menuFormSave(){
    //保存用户菜单跟用户权限,只要userId，以及Id集合就可以了
    let menuIdList = [];
    for (let check of tree.getChecked('roleMenuTree')[0].children) {
        menuIdList.push(check.id);
        if (check.children && check.children.length > 0) {
            for (let check1 of check.children) {
                menuIdList.push(check1.id);
            }
        }
    }
    let postData = {
        roleid: $("#roleid").val(),
        menuIdList: menuIdList.join(",")
    };
    $.post(ctx + "/manage/sys/role/saveRoleMenu", postData, function (data) {layer.msg("保存成功", {icon: 1,time: 2000}, function () {});});
}
/**
 * 加载权限
 */
function loadAuthorityTree(roleid) {
    //获取菜单数据
    $.post(ctx + "/manage/sys/role/tree", {"roleid":roleid}, function (data) {
        //数据说明：id对应id，title对应menuName，href对应menuPath
        let treeData = commonUtil.updateKeyForLayuiTree(data.data);
        console.log(treeData);
        //开启节点操作图标
        tree.render({
            elem: '#roleMenuTree'
            , id: 'roleMenuTree'
            , data: [{
                title: '系统权限根节点'
                , href: "/"
                , id: 0
                , spread: true
                , children: treeData
            }]
            , showCheckbox: true
        });
    });
}