package com.wisedu.mobile.common.service.impl;

import com.wisedu.mobile.common.BaseServiceImpl;
import com.wisedu.mobile.common.domain.SysDict;
import com.wisedu.mobile.common.service.ISysDictService;
import org.springframework.stereotype.Service;

/**
 * @Author: hj
 * @Date: 2020/4/22 下午1:37
 */
@Service
public class SysDictServiceImpl extends BaseServiceImpl<SysDict> implements ISysDictService {
}
