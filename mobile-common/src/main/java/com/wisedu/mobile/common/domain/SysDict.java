package com.wisedu.mobile.common.domain;

import com.wisedu.mobile.common.BaseEntity;
import org.nutz.dao.entity.annotation.*;
import org.nutz.plugins.validation.annotation.Validations;

import java.io.Serializable;

/**
 * 字典表 sys_dict
 *
 * @author hj
 * @date 2019-04-16
 */
@Table("sys_dict")
@Comment("字典表")
public class SysDict extends BaseEntity implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     * 数据值
     */
    @Column("dict_val")
    @Comment("数据值 ")
    @Validations(required=true,errorMsg = "数据值不能为空")
    private String dictVal;

    /**
     * 标签名
     */
    @Column("dict_label")
    @Comment("标签名 ")
    @Validations(required=true,errorMsg = "标签名不能为空")
    private String dictLabel;

    /**
     * 类型
     */
    @Column("dict_type")
    @Comment("类型 ")
    private String dictType;
    /**
     * 排序
     */
    @Column("order_num")
    @Comment("显示顺序")
    @Validations(required=true,errorMsg = "排序不能为空")
    @ColDefine(type = ColType.VARCHAR, width = 10)
    private String orderNum;
    /**
     * 父级编号
     */
    @Column("parent_id")
    @Comment("父级编号 ")
    private String parentId;

    /**
     * 删除标记
     */
    @Column("status")
    @Comment("删除标记")
    @ColDefine(type = ColType.VARCHAR, width = 10)
    private int status;

    public String getDictVal() {
        return dictVal;
    }

    public void setDictVal(String dictVal) {
        this.dictVal = dictVal;
    }

    public String getDictLabel() {
        return dictLabel;
    }

    public void setDictLabel(String dictLabel) {
        this.dictLabel = dictLabel;
    }

    public String getDictType() {
        return dictType;
    }

    public void setDictType(String dictType) {
        this.dictType = dictType;
    }

    public String getOrderNum() {
        return orderNum;
    }

    public void setOrderNum(String orderNum) {
        this.orderNum = orderNum;
    }

    public String getParentId() {
        return parentId;
    }

    public void setParentId(String parentId) {
        this.parentId = parentId;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }
}
