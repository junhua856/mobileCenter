package com.wisedu.mobile.manage.config;

import com.github.threefish.spring.sqltpl.templte.beetl.BeetlSqlTemplteEngineImpl;
import org.nutz.lang.Strings;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;

/**
 * @Author: hj
 * @Date: 2020/3/18 上午9:17
 */
@Component
public class DaoConfig {
    @Bean
    public BeetlSqlTemplteEngineImpl sqlTemplteEngine() {
        BeetlSqlTemplteEngineImpl beetlSqlTemplteEngine = new BeetlSqlTemplteEngineImpl();
        beetlSqlTemplteEngine.getGt().registerFunctionPackage("S", Strings.class);
        return beetlSqlTemplteEngine;
    }
}
