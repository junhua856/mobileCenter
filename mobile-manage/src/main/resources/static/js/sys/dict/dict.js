let tableIns;
let tree;
layui.use(['element', 'form', 'table', 'layer', 'laydate', 'tree', 'util'], function () {
    let table = layui.table;
    let form = layui.form;//select、单选、复选等依赖form
    let element = layui.element; //导航的hover效果、二级菜单等功能，需要依赖element模块
    let laydate = layui.laydate;
    tree = layui.tree;
    let height = document.documentElement.clientHeight - 160;

    tableIns = table.render({
        elem: '#table'
        , url: ctx + '/manage/sys/dict/page'
        , method: 'POST'
        //请求前参数处理
        , request: {
            pageName: 'pageNumber' //页码的参数名称，默认：page
            , limitName: 'pageSize' //每页数据量的参数名，默认：limit
        }
        , response: {
            statusName: 'code' //规定数据状态的字段名称，默认：code
            , statusCode: 200 //规定成功的状态码，默认：0
            , msgName: 'msg' //规定状态信息的字段名称，默认：msg
        }
        //响应后数据处理
        ,parseData: function (res) { //res 即为原始返回的数据
            return {
                "code": res.code, //解析接口状态
                "msg": res.msg, //解析提示文本
                "count": res.data.total, //解析数据长度
                "data": res.data.rows //解析数据列表
            };
        }
        , toolbar: '#tableToolbar'
        , title: '字典列表'
        , cols: [[
            {field: 'id', title: 'ID', hide: true}
            , {field: 'dictType', title: '字典类型'}
            , {field: 'status', title: '状态'}
            , {field: 'dictLabel', title: '字典标签'}
            , {field: 'dictVal', title: '数据值'}
            , {fixed: 'right', title: '操作', toolbar: '#tableBar'}
        ]]
        , defaultToolbar: ['', '', '']
        , page: true
        , height: height
        , cellMinWidth: 80
    });

    //头工具栏事件
    table.on('toolbar(test)', function (obj) {
        switch (obj.event) {
            case 'addData':
                //重置操作表单
                $("#form")[0].reset();
                form.render();
                layer.msg("请填写右边的表单并保存！");
                break;
            case 'query':
                let queryByDictType = $("#queryByDictType").val();
                let query = {
                    page: {
                        curr: 1 //重新从第 1 页开始
                    }
                    , done: function (res, curr, count) {
                        //完成后重置where，解决下一次请求携带旧数据
                        this.where = {};
                    }
                };
                if (queryByDictType) {
                    //设定异步数据接口的额外参数
                    query.where = {dictType: queryByDictType};
                }
                tableIns.reload(query);
                $("#queryByDictType").val(queryByDictType);
                break;
        }
    });

    //监听行工具事件
    table.on('tool(test)', function (obj) {
        let data = obj.data;
        //删除
        if (obj.event === 'del') {
            layer.confirm('确认删除吗？', function (index) {
                //向服务端发送删除指令
                $.delete(ctx + "/manage/sys/dict/delete/" + data.id, {}, function (data) {
                    obj.del();
                    layer.close(index);
                })
            });
        }
        //编辑
        else if (obj.event === 'edit') {
            //回显操作表单
            $("#form").form(data);
            form.render();
        }else{

        }

    });

});

/**
 * 提交保存
 */
function dictFormSave() {
    let dictForm = $("#form").serializeObject();
    $.post(ctx + "/manage/sys/dict/save", dictForm, function (data) {
        layer.msg("保存成功", {icon: 1,time: 2000}, function () {});
        tableIns.reload();
    });
}
