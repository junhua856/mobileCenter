package com.wisedu.mobile.common.domain;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.wisedu.mobile.common.BaseEntity;
import org.nutz.dao.entity.annotation.*;
import org.nutz.plugins.validation.annotation.Validations;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;
import java.util.Date;
import java.util.List;
import java.util.Set;

/**
 * 用户对象 sys_user
 * 
 * @author hj
 */
@Table("sys_user")
@Comment("用户表")
public class SysUser extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /**
     * 部门ID
     */
    @Column("dept_id")
    @Comment("部门ID")
    @Validations(required=true,errorMsg = "部门不能为空")
    @ColDefine(type = ColType.VARCHAR, width = 64)
    private String deptId;

    /**
     * 部门父ID
     */
    private String parentId;

    /**
     * 登录名称
     */
    @Column("login_name")
    @Comment("登录名称")
    @Validations(required=true,errorMsg = "登录名称不能为空")
    @ColDefine(type = ColType.VARCHAR, width = 50)
    private String loginName;

    /**
     * 用户名称
     */
    @Column("user_name")
    @Comment("用户名称")
    @Validations(required=true,errorMsg = "用户名称不能为空")
    @ColDefine(type = ColType.VARCHAR, width = 100)
    private String userName;

    /**
     * 密码
     */
    @Column("password")
    @Comment("密码")
    @Validations(required=true,errorMsg = "密码不能为空")
    @ColDefine(type = ColType.VARCHAR, width = 100)
    private String password;


    /**
     * 用户邮箱
     */
    @Column("email")
    @Comment("用户邮箱")
    @ColDefine(type = ColType.VARCHAR, width = 100)
    private String email;

    /**
     * 手机号码
     */
    @Column("phonenumber")
    @Comment("手机号码")
    @ColDefine(type = ColType.VARCHAR, width = 50)
    private String phonenumber;

    /**
     * 用户性别
     */
    @Column
    @Comment("用户性别")
    @ColDefine(type = ColType.VARCHAR, width = 10)
    private String gender;

    /**
     * 用户头像
     */
    @Column
    @Comment("用户头像")
    @ColDefine(type = ColType.VARCHAR, width = 100)
    private String avatar;

    /**
     * 主题
     */
    @Column
    @Comment("主题")
    @ColDefine(type = ColType.VARCHAR, width = 10)
    private String theme = "blue";

    /**
     * 语言
     */
    @Column
    @Comment("语言")
    @ColDefine(type = ColType.VARCHAR, width = 10)

    private String local = "zh_CN";

    /**
     * 帐号状态（0正常 1停用）
     */
    @Column
    @Comment("帐号状态（0正常 1停用） ")
    @ColDefine(type = ColType.VARCHAR, width = 10)
    private String status = "0";

    /**
     * 部门对象
     */
    @One(field = "deptId")
    private SysDept dept;

    /**
     * 角色集合
     */
    @ManyMany(from = "user_id", relation = "sys_user_role", to = "role_id")
    private List<SysRole> roles;

    /** 角色组 */
    private String[] roleIds;

    /** 岗位组 */
    private String[] postIds;



    public SysUser()
    {

    }



    public boolean isAdmin()
    {
        return isAdmin(getId());
    }

    public static boolean isAdmin(String userId)
    {
        return "1".equals(userId);
    }


    public String getDeptId() {
        return deptId;
    }

    public void setDeptId(String deptId) {
        this.deptId = deptId;
    }

    public String getParentId() {
        return parentId;
    }

    public void setParentId(String parentId) {
        this.parentId = parentId;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhonenumber() {
        return phonenumber;
    }

    public void setPhonenumber(String phonenumber) {
        this.phonenumber = phonenumber;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public String getTheme() {
        return theme;
    }

    public void setTheme(String theme) {
        this.theme = theme;
    }

    public String getLocal() {
        return local;
    }

    public void setLocal(String local) {
        this.local = local;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }


    public SysDept getDept() {
        return dept;
    }

    public void setDept(SysDept dept) {
        this.dept = dept;
    }

    public List<SysRole> getRoles() {
        return roles;
    }

    public void setRoles(List<SysRole> roles) {
        this.roles = roles;
    }

    public String[] getRoleIds() {
        return roleIds;
    }

    public void setRoleIds(String[] roleIds) {
        this.roleIds = roleIds;
    }

    public String[] getPostIds() {
        return postIds;
    }

    public void setPostIds(String[] postIds) {
        this.postIds = postIds;
    }

    public String getLoginName() {
        return loginName;
    }

    public void setLoginName(String loginName) {
        this.loginName = loginName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
