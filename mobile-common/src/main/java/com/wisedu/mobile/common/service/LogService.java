package com.wisedu.mobile.common.service;

import com.wisedu.mobile.common.domain.HttpTraceLog;
import com.wisedu.mobile.common.domain.Syslog;
import com.wisedu.mobile.common.utils.ServletUtils;
import eu.bitwalker.useragentutils.UserAgent;
import org.nutz.json.Json;
import org.nutz.lang.Lang;
import org.nutz.lang.Times;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;

import java.util.Date;

/**
 * @Author: hj
 * @Date: 2020/3/25 下午1:10
 */
@Component
public class LogService {
    private static final Logger sys_user_logger = LoggerFactory.getLogger("sys-user");

    private static final Logger sys_login_logger = LoggerFactory.getLogger("sys-login");

    private static final Logger sys_url_logger = LoggerFactory.getLogger("sys-url");

    /**
     * 记录登陆信息
     *
     * @param username 用户名
     * @param status 状态
     * @param message 消息
     * @return 任务task
     */
    @Async
    public  void recordLogininfor(final String username, final String status, final String message,final String ip,final String os,final String browser)
    {
                // 打印信息到日志
                sys_login_logger.info(Times.sDT(new Date())+"|"+username+"|"+status+"|"+message+"|"+ip+"|"+os+"|"+browser);
    }

    /**
     * 操作日志记录
     *
     * @param operLog 操作日志信息
     * @return 任务task
     */
    @Async
    public  void recordOper(final Syslog operLog)
    {
                sys_user_logger.info(Json.toJson(operLog));
    }

    /**
     * 操作接口记录
     *
     * @param traceLog 操作日志信息
     * @return 任务task
     */
    @Async
    public  void recordTrace(final HttpTraceLog traceLog)
    {
                sys_url_logger.info("Http trace log: {}", Json.toJson(traceLog));
    }
}
