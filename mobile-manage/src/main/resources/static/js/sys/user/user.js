let tableIns;
let tree;
layui.use(['element', 'form', 'table', 'layer', 'laydate', 'tree', 'util'], function () {
    let table = layui.table;
    let form = layui.form;//select、单选、复选等依赖form
    let element = layui.element; //导航的hover效果、二级菜单等功能，需要依赖element模块
    let laydate = layui.laydate;
    tree = layui.tree;
    let height = document.documentElement.clientHeight - 160;

    tableIns = table.render({
        elem: '#userTable'
        , url: ctx + '/manage/sys/user/page'
        , method: 'POST'
        //请求前参数处理
        , request: {
            pageName: 'pageNumber' //页码的参数名称，默认：page
            , limitName: 'pageSize' //每页数据量的参数名，默认：limit
        }
        , response: {
            statusName: 'code' //规定数据状态的字段名称，默认：code
            , statusCode: 200 //规定成功的状态码，默认：0
            , msgName: 'msg' //规定状态信息的字段名称，默认：msg
        }
        //响应后数据处理
        ,parseData: function (res) { //res 即为原始返回的数据
            return {
                "code": res.code, //解析接口状态
                "msg": res.msg, //解析提示文本
                "count": res.data.total, //解析数据长度
                "data": res.data.rows //解析数据列表
            };
        }
        , toolbar: '#userTableToolbarDemo'
        , title: '用户列表'
        , cols: [[
            {field: 'id', title: 'ID', hide: true}
            , {field: 'loginName', title: '登录名'}
            , {field: 'status', title: '状态'}
            , {field: 'local', title: '语言'}
            , {field: 'theme', title: '主题'}
            , {fixed: 'right', title: '操作', toolbar: '#userTableBarDemo'}
        ]]
        , defaultToolbar: ['', '', '']
        , page: true
        , height: height
        , cellMinWidth: 80
    });

    //头工具栏事件
    table.on('toolbar(test)', function (obj) {
        switch (obj.event) {
            case 'addData':
                $("#userForm").show();
                $("#userAuthorityForm").hide();
                //重置操作表单
                $("#userForm")[0].reset();
                form.render();
                layer.msg("请填写右边的表单并保存！");
                break;
            case 'query':
                let queryByUserName = $("#queryByUserName").val();
                let query = {
                    page: {
                        curr: 1 //重新从第 1 页开始
                    }
                    , done: function (res, curr, count) {
                        //完成后重置where，解决下一次请求携带旧数据
                        this.where = {};
                    }
                };
                if (queryByUserName) {
                    //设定异步数据接口的额外参数
                    query.where = {userName: queryByUserName};
                }
                tableIns.reload(query);
                $("#queryByUserName").val(queryByUserName);
                break;
        }
    });

    //监听行工具事件
    table.on('tool(test)', function (obj) {
        let data = obj.data;
        //删除
        if (obj.event === 'del') {
            layer.confirm('确认删除吗？', function (index) {
                //向服务端发送删除指令
                $.delete(ctx + "/manage/sys/user/delete/" + data.userId, {}, function (data) {
                    obj.del();
                    layer.close(index);
                })
            });
        }
        //编辑
        else if (obj.event === 'edit') {
            $("#userForm").show();
            $("#userAuthorityForm").hide();
            //回显操作表单
            $("#userForm").form(data);
            form.render();
        }
        //授权
        else if (obj.event === 'auth') {
            $("#userForm").hide();
            $("#userAuthorityForm").show();
            $("#userid").val(data.id);
            loadAuthorityTree(data.id);
        }
    });

    //日期选择器
    laydate.render({
        elem: '#expiredTimeDate',
        format: "yyyy-MM-dd HH:mm:ss"
    });
});

/**
 * 提交保存
 */
function userFormSave() {
    let userForm = $("#userForm").serializeObject();
    userForm.removeAttr("");
    console.log(userForm);
    $.post(ctx + "/manage/sys/user/save", userForm, function (data) {
        layer.msg("保存成功", {icon: 1,time: 2000}, function () {});
        tableIns.reload();
    });
}


function userAuthorityFormSave(){
    //保存用户菜单跟用户权限,只要userId，以及Id集合就可以了
    let menuIdList = [];
    for (let check of tree.getChecked('userAuthorityTree')[0].children) {
        menuIdList.push(check.id);
        if (check.children && check.children.length > 0) {
            for (let check1 of check.children) {
                menuIdList.push(check1.id);
            }
        }
    }
    let postData = {
        userid: $("#userid").val(),
        roleIdList: menuIdList.join(",")
    };
    $.post(ctx + "/manage/sys/user/saveUserRole", postData, function (data) {layer.msg("保存成功", {icon: 1,time: 2000}, function () {});});
}
/**
 * 加载权限
 */
function loadAuthorityTree(userid) {
    //获取菜单数据
    $.post(ctx + "/manage/sys/user/tree", {"userid":userid}, function (data) {
        //数据说明：id对应id，title对应menuName，href对应menuPath
        let treeData = commonUtil.updateKeyForLayuiTree(data.data);

        //开启节点操作图标
        tree.render({
            elem: '#userAuthorityTree'
            , id: 'userAuthorityTree'
            , data: [{
                title: '系统权限根节点'
                , href: "/"
                , id: 0
                , spread: true
                , children: treeData
            }]
            , showCheckbox: true
        });
    });
}