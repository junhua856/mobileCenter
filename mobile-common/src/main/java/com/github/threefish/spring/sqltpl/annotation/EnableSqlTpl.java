package com.github.threefish.spring.sqltpl.annotation;

import com.github.threefish.spring.sqltpl.SqlXmlBeanPostProcessor;
import org.springframework.context.annotation.Import;

import java.lang.annotation.*;

/**
 * @author 黄川 huchuc@vip.qq.com
 * date 2020/3/11
 */
@Target({ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Import({SqlXmlBeanPostProcessor.class})
public @interface EnableSqlTpl {

}
