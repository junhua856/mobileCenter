package com.wisedu.mobile.auth.service.impl.v1.db;

import com.wisedu.mobile.common.BaseServiceImpl;
import com.wisedu.mobile.common.domain.SysMenu;
import com.wisedu.mobile.common.domain.SysRole;
import com.wisedu.mobile.common.domain.SysUser;
import com.wisedu.mobile.common.service.ISysUserService;
import org.nutz.dao.Cnd;
import org.nutz.dao.FieldFilter;
import org.nutz.dao.util.Daos;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

/**
 * @Author: hj
 * @Date: 2020/3/17 上午9:18
 */
@Service
public class SysUserServiceImpl extends BaseServiceImpl<SysUser> implements ISysUserService {

    @Override
    public SysUser selectUserByLoginName(String loginname) {
        return fetch(Cnd.where("loginName","=",loginname));
    }

    @Override
    public SysUser selectRoleByUser(SysUser user) {
        if(user!=null){
            fetchLinks(user,"roles");
            if(user.getRoles()!=null){
                fetchLinks(user.getRoles(),"menus");
            }
        }
        return user;
    }

}
