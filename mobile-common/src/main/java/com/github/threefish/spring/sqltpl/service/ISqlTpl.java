package com.github.threefish.spring.sqltpl.service;

import com.github.threefish.spring.sqltpl.SqlTplHolder;

/**
 * @author 黄川 huchuc@vip.qq.com
 * date 2020/3/11
 */
public interface ISqlTpl {
    /**
     * @return 取得sql模版持有对象
     */
    SqlTplHolder getSqlTplHolder();

    /**
     * 设置sql模版持有对象
     *
     * @param sqlTplHolder
     */
    void setSqlTpl(SqlTplHolder sqlTplHolder);
}
