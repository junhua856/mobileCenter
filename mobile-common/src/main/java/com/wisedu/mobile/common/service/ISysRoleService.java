package com.wisedu.mobile.common.service;



import com.wisedu.mobile.common.domain.SysRole;
import org.nutz.dao.entity.Record;

import java.util.List;
import java.util.Set;

/**
 * 角色业务层
 * 
 * @author hj
 */
public interface ISysRoleService
{
    /**
     * 根据用户查询角色
     *
     * @return 角色列表
     */
    public List<Record> selectRoleByUser(String userid);
}
