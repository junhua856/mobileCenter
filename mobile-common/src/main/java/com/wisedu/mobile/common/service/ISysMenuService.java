package com.wisedu.mobile.common.service;


import com.wisedu.mobile.common.domain.SysMenu;
import org.nutz.dao.entity.Record;

import java.util.List;
import java.util.Map;

/**
 * 菜单 业务层
 * 
 * @author hj
 */
public interface ISysMenuService
{
    /**
     * 查询系统配置了限流的菜单
     *
     * @return 菜单列表
     */
    public Map<String,SysMenu> selectLimiterMenu();

    /**
     * 根据角色查询菜单
     *
     * @return 菜单列表
     */
    public List<Record> selectMenuByRole(String roleid);

    /**
     * 查询系统配置了2k的菜单
     *
     * @return 菜单列表
     */
    public  Map<String,SysMenu> select2kMenu();
}
