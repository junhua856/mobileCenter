package com.wisedu.mobile.common.exception;


import com.wisedu.mobile.common.BaseException;

/**
 * 用户信息异常类
 * 
 * @author hj
 */
public class UserException extends BaseException
{
    private static final long serialVersionUID = 1L;

    public UserException(String code, Object[] args)
    {
        super("user", code, args, null);
    }
}
