package com.wisedu.mobile.core.config;

import com.wisedu.mobile.common.constant.Constants;
import com.wisedu.mobile.core.common.MyLocaleResolver;
import com.wisedu.mobile.core.interceptor.AbstractRepeatSubmitInterceptor;
import com.wisedu.mobile.core.interceptor.EnhanceInterceptor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.LocaleResolver;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

/**
 * 通用配置
 * 
 * @author hj
 */
@Configuration
public class ResourcesConfig implements WebMvcConfigurer
{
    @Autowired
    private AbstractRepeatSubmitInterceptor repeatSubmitInterceptor;
    @Autowired
    private EnhanceInterceptor enhanceInterceptor;


    @Override
    public void addResourceHandlers(ResourceHandlerRegistry registry)
    {
        /** 本地文件上传路径 */
        registry.addResourceHandler(Constants.RESOURCE_PREFIX + "/**").addResourceLocations("file:" + MobileCenterConfig.getProfile() + "/");

        /** swagger配置 */
        registry.addResourceHandler("swagger-ui.html").addResourceLocations("classpath:/META-INF/resources/");
        registry.addResourceHandler("/webjars/**").addResourceLocations("classpath:/META-INF/resources/webjars/");
    }

    /**
     * 自定义拦截规则
     */
    @Override
    public void addInterceptors(InterceptorRegistry registry)
    {
        registry.addInterceptor(repeatSubmitInterceptor).addPathPatterns("/**");
        registry.addInterceptor(enhanceInterceptor).addPathPatterns("/**");
    }

    /**
     * 注册自定义的LocaleResolver
     */
    @Bean
    public LocaleResolver localeResolver(){
        return new MyLocaleResolver();
    }
}