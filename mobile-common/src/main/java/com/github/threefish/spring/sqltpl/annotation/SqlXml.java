package com.github.threefish.spring.sqltpl.annotation;

import java.lang.annotation.*;

/**
 * @author 黄川 huchuc@vip.qq.com
 * date 2020/3/11
 */
@Target({ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Inherited
public @interface SqlXml {

    /**
     * 文件相对与当前service类路径
     * <p>
     * 默认为java文件名以.xml结尾 如（UserServicesImpl.java 对应 UserServicesImpl.xml）
     *
     * @return xml文件路径
     */
    String value() default "";

}
