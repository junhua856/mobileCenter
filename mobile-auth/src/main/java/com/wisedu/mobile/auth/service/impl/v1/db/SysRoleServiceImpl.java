package com.wisedu.mobile.auth.service.impl.v1.db;

import com.github.threefish.spring.sqltpl.annotation.SqlXml;
import com.wisedu.mobile.common.BaseServiceImpl;
import com.wisedu.mobile.common.domain.SysRole;
import com.wisedu.mobile.common.domain.SysUser;
import com.wisedu.mobile.common.service.ISysRoleService;
import org.nutz.dao.entity.Record;
import org.springframework.stereotype.Service;

import java.util.*;

/**
 * @Author: hj
 * @Date: 2020/3/17 上午11:00
 */
@Service
@SqlXml
public class SysRoleServiceImpl extends BaseServiceImpl<SysRole> implements ISysRoleService {

    @Override
    public List<Record> selectRoleByUser(String userid) {
        Map param = new HashMap();
        param.put("userid",userid);
        return list("selectRoleByUser",null,param);
    }
}
