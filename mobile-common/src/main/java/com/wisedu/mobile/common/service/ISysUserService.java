package com.wisedu.mobile.common.service;



import com.wisedu.mobile.common.domain.SysUser;

import java.util.List;

/**
 * 用户 业务层
 * 
 * @author hj
 */
public interface ISysUserService
{

    /**
     * 通过用户名查询用户
     * 
     * @param loginname 用户名
     * @return 用户对象信息
     */
    public SysUser selectUserByLoginName(String loginname);


    /**
     * 根据用户查询用户所属角色组
     * 
     * @param user 用户
     * @return 结果
     */
    public SysUser selectRoleByUser(SysUser user);



}
