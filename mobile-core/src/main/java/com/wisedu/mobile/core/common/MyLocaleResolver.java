package com.wisedu.mobile.core.common;

import com.wisedu.mobile.common.domain.LoginUser;
import com.wisedu.mobile.common.domain.SysUser;
import com.wisedu.mobile.common.service.TokenService;
import com.wisedu.mobile.common.utils.SpringUtils;
import org.springframework.web.servlet.LocaleResolver;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Locale;
/**
 * @Author: hj
 * @Date: 2020/3/17 下午6:40
 */
public class MyLocaleResolver implements LocaleResolver {

    @Override
    public Locale resolveLocale(HttpServletRequest httpServletRequest) {
        LoginUser loginUser = ((TokenService)SpringUtils.getBean("tokenService")).getLoginUser(httpServletRequest);
        Locale locale = Locale.getDefault();

        if(loginUser!=null) {
            SysUser user = loginUser.getUser();
            if (user != null && user.getLocal() != null) {
                String[] split = user.getLocal().split("_");
                locale = new Locale(split[0], split[1]);
            }
        }

        return locale;
    }

    @Override
    public void setLocale(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, Locale locale) {

    }
}

