package com.wisedu.mobile.core.config;


import com.github.xiaoymin.knife4j.spring.annotations.EnableKnife4j;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import springfox.bean.validators.configuration.BeanValidatorPluginsConfiguration;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

/**
 *  swagger2配置 默认地址http://localhost:8080/swagger-ui.html
 * @author hj
 * @date 21:05 2018/3/17
 */
@Configuration
@EnableSwagger2
@EnableKnife4j
@Import(BeanValidatorPluginsConfiguration.class)
public class SwaggerConfig {


	private ApiInfo apiInfo() {
		return new ApiInfoBuilder()
				.title("认证端Restful APIs")
				.description("restful apis docs")
				.termsOfServiceUrl("wisedu.com").version("1.0").build();
	}

	@Bean(value = "defaultApi2")
	public Docket defaultApi2() {
		Docket docket=new Docket(DocumentationType.SWAGGER_2)
				.apiInfo(apiInfo())
				.groupName("认证v1.0版本")
				.select()
				.apis(RequestHandlerSelectors.basePackage("com.wisedu.mobile.auth.controller"))
				//.apis(RequestHandlerSelectors.withMethodAnnotation(ApiOperation.class))
				.paths(PathSelectors.any())
				.build();
		return docket;
	}
/*
	@Bean(value = "defaultApi")
	public Docket defaultApi() {
		ParameterBuilder parameterBuilder=new ParameterBuilder();
		List<Parameter> parameters= Lists.newArrayList();
		parameterBuilder.name("token").description("token令牌").modelRef(new ModelRef("String"))
				.parameterType("header")
				.required(true).build();
		parameters.add(parameterBuilder.build());

		Docket docket=new Docket(DocumentationType.SWAGGER_2)
				.apiInfo(apiInfo())
				.groupName("默认接口")
				.select()
				.apis(RequestHandlerSelectors.basePackage("com.swagger.bootstrap.ui.demo.controller"))
				//.apis(RequestHandlerSelectors.withMethodAnnotation(ApiOperation.class))
				.paths(PathSelectors.any())
				.build().globalOperationParameters(parameters)
				.securityContexts(Lists.newArrayList(securityContext())).securitySchemes(Lists.<SecurityScheme>newArrayList(apiKey()));
		return docket;
	}
	*/

}
