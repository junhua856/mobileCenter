package com.wisedu.mobile.common.domain;


import com.wisedu.mobile.common.BaseEntity;
import org.nutz.dao.entity.annotation.*;
import org.nutz.plugins.validation.annotation.Validations;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;
import java.util.ArrayList;
import java.util.List;

/**
 * 部门表 sys_dept
 * 
 * @author hj
 */
@Table("sys_dept")
@Comment("部门表")
public class SysDept extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /**
     * 父部门ID
     */
    @Column("parent_id")
    @Comment("父部门ID")
    @ColDefine(type = ColType.VARCHAR, width = 64)
    private String parentId;


    /**
     * 部门名称
     */
    @Column("dept_name")
    @Comment("部门名称")
    @Validations(required=true,errorMsg = "部门名称不能为空")
    @ColDefine(type = ColType.VARCHAR, width = 100)
    private String deptName;

    /**
     * 显示顺序
     */
    @Column("order_num")
    @Comment("显示顺序")
    @Validations(required=true,errorMsg = "显示顺序不能为空")
    @ColDefine(type = ColType.VARCHAR, width = 10)
    private int orderNum;

    /**
     * 负责人
     */
    @Column("leader")
    @Comment("负责人")
    @ColDefine(type = ColType.VARCHAR, width = 100)
    private String leader;

    /**
     * 联系电话
     */
    @Column("phonenumber")
    @Comment("联系电话")
    @ColDefine(type = ColType.VARCHAR, width = 50)
    private String phonenumber;
    /**
     * 邮箱
     */
    @Column("email")
    @Comment("邮箱")
    @ColDefine(type = ColType.VARCHAR, width = 100)
    private String email;

    /**
     * 状态（0正常 1停用）
     */
    @Column("status")
    @Comment("状态（0正常 1停用） ")
    @ColDefine(type = ColType.VARCHAR, width = 10)
    private boolean status;


    /** 父部门名称 */
    private String parentName;
    
    /** 子部门 */
    private List<SysDept> children = new ArrayList<SysDept>();


    public String getParentId() {
        return parentId;
    }

    public void setParentId(String parentId) {
        this.parentId = parentId;
    }

    public String getDeptName() {
        return deptName;
    }

    public void setDeptName(String deptName) {
        this.deptName = deptName;
    }

    public int getOrderNum() {
        return orderNum;
    }

    public void setOrderNum(int orderNum) {
        this.orderNum = orderNum;
    }

    public String getLeader() {
        return leader;
    }

    public void setLeader(String leader) {
        this.leader = leader;
    }

    public String getPhonenumber() {
        return phonenumber;
    }

    public void setPhonenumber(String phonenumber) {
        this.phonenumber = phonenumber;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public String getParentName() {
        return parentName;
    }

    public void setParentName(String parentName) {
        this.parentName = parentName;
    }

    public List<SysDept> getChildren() {
        return children;
    }

    public void setChildren(List<SysDept> children) {
        this.children = children;
    }
}
