package com.github.threefish.spring.sqltpl.service;

import java.util.Map;

/**
 * @author 黄川 huchuc@vip.qq.com
 * date: 2020/3/11
 */
public interface ISqlTemplteEngine {

    /**
     * 解析模版
     *
     * @param source   源
     * @param bindData 绑定参数
     * @return 渲染后的SQL
     */
    String render(String source, Map bindData);

}
