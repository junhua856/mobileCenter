package com.wisedu.mobile.manage.service;

import com.github.threefish.spring.sqltpl.SqlTplHolder;
import com.github.threefish.spring.sqltpl.annotation.SqlXml;
import com.github.threefish.spring.sqltpl.service.ISqlTpl;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;
/**
 * @Author: hj
 * @Date: 2020/3/18 上午9:27
 */
@Service
@SqlXml
public class SqlDao implements ISqlTpl {

    private SqlTplHolder tplHolder;

    public String selectOne() {
        return tplHolder.getOriginalTemplate("selectOne");
    }


    public String selectTwo() {
        Map<String, Object> stringObjectMap = new HashMap<>(1);
        stringObjectMap.put("name","测试");
        stringObjectMap.put("age",18);
        return tplHolder.getSql("selectTwo", stringObjectMap);
    }

    @Override
    public SqlTplHolder getSqlTplHolder() {
        return tplHolder;
    }

    @Override
    public void setSqlTpl(SqlTplHolder sqlsTplHolder) {
        this.tplHolder = sqlsTplHolder;
    }
}
