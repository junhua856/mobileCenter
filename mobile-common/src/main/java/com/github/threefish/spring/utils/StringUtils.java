package com.github.threefish.spring.utils;

/**
 * @author 黄川 huchuc@vip.qq.com
 * date: 2020/3/11
 */
public class StringUtils {


    /**
     * 使用给定的分隔符, 将一个数组拼接成字符串
     *
     * @param sp    分隔符
     * @param array 要拼接的数组
     * @return 拼接好的字符串
     */
    public static <T> String join(String sp, T... array) {
        StringBuilder sb = new StringBuilder();
        if (null == array || 0 == array.length) {
            return sb.toString();
        }
        sb.append(array[0]);
        for (int i = 1; i < array.length; i++) {
            sb.append(sp).append(array[i]);
        }
        return sb.toString();
    }


    /**
     * 如果此字符串为 null 或者全为空白字符，则返回 true
     *
     * @param cs 字符串
     * @return 如果此字符串为 null 或者全为空白字符，则返回 true
     */
    public static boolean isBlank(CharSequence cs) {
        if (null == cs) {
            return true;
        }
        int length = cs.length();
        for (int i = 0; i < length; i++) {
            if (!(Character.isWhitespace(cs.charAt(i)))) {
                return false;
            }
        }
        return true;
    }

    public static boolean isNotBlank(CharSequence cs) {
        return !isBlank(cs);
    }

}
