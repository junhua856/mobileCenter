let tree = {};
layui.use(['element', 'form', 'table', 'layer', 'tree', 'util'], function () {
    let form = layui.form;//select、单选、复选等依赖form
    let element = layui.element; //导航的hover效果、二级菜单等功能，需要依赖element模块
    tree = layui.tree;

    //获取菜单数据
    $.post(ctx + "/manage/sys/dept/tree", {}, function (data) {
        //数据说明：id对应id，title对应deptName，href对应deptPath
        let treeData = commonUtil.updateKeyForLayuiTree(data.data);

        //开启节点操作图标
        tree.render({
            elem: '#deptTree'
            , id: 'deptTree'
            , data: [{
                title: '系统菜单根节点'
                , href: "/"
                , id: "0"
                , spread: true
                , children: treeData
            }]
            , onlyIconControl: true
            , edit: ['add', 'del']
            //节点被点击
            , click: function (obj) {
                //回显操作表单，说明：deptId对应id，title对应deptName，href对应deptPath
                if(obj.data.id==''){
                    $("#deptForm").form({
                        deptName: obj.data.title,
                        parentName: obj.elem.parent().parent().children(".layui-tree-entry").find(".layui-tree-txt").text(),
                        parentId: obj.elem.parent().parent().data("id"),
                        treeId: obj.data.id
                    });
                }else {
                    $.get(ctx + "/manage/sys/dept/get/" + obj.data.id, function (data) {
                        let d = $.extend(data.data, {
                            parentName: obj.elem.parent().parent().children(".layui-tree-entry").find(".layui-tree-txt").text(),
                            parentId: obj.elem.parent().parent().data("id"),
                            treeId: obj.data.id
                        });
                        $("#deptForm").form(d);
                        form.render();
                    })
                }
            }
            //复选框被点击
            , oncheck: function (obj) {
                console.log(obj.data); //得到当前点击的节点数据
                console.log(obj.checked); //得到当前节点的展开状态：open、close、normal
                console.log(obj.elem); //得到当前节点元素
            }
            //对节点进行增删改操作回调
            , operate: function (obj) {
                let type = obj.type; //得到操作类型：add、edit、del
                let data = obj.data; //得到当前节点的数据
                let elem = obj.elem; //得到当前节点元素

                if (type === 'add') { //增加节点
                    $("#deptForm")[0].reset();
                    //返回 key 值
                    return "";
                } else if (type === 'del') { //删除节点
                    layer.confirm('确认要删除这个菜单吗？注意：删除父节点将会一同删除子节点', function (index) {
                        $.delete(ctx + "/manage/sys/dept/delete/" + data.id,{}, function () {
                            layer.msg("删除成功");
                            elem.remove();
                        });
                        layer.close(index);
                    });

                }
            }
        });
    });
});


/**
 * 提交保存
 */
function deptFormSave() {
    var deptForm = $("#deptForm").serializeObject();
    if(deptForm.id === "0"){
        layer.msg("根节点仅用于展示，不可操作！", {icon: 2,time: 2000}, function () {});
        return;
    }
    if(deptForm.parentId === "0"){
        deptForm.parentId = "";
    }
    $.post(ctx + "/manage/sys/dept/save", deptForm, function (data) {
        layer.msg(data.msg, {icon: 1,time: 2000}, function () {});

        //更新树组件
        $("div[data-id='" + deptForm.treeId + "']").children(".layui-tree-entry").find(".layui-tree-txt").text(deptForm.deptName);
        $("div[data-id='" + deptForm.treeId + "']").attr("data-id", deptForm.id);
    });
}