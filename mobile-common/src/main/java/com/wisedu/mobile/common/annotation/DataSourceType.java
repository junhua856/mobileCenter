package com.wisedu.mobile.common.annotation;

/**
 * 数据源
 * 
 * @author hj
 */
public enum DataSourceType
{
    /**
     * 主库
     */
    MASTER,

    /**
     * 从库
     */
    SLAVE
}
