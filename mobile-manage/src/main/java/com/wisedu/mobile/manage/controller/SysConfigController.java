package com.wisedu.mobile.manage.controller;

import com.wisedu.mobile.common.BaseController;
import com.wisedu.mobile.common.domain.SysConfig;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @Author: hj
 * @Date: 2020/3/24 下午4:28
 */
@RestController
@RequestMapping("/manage/sys/config")
public class SysConfigController extends BaseController<SysConfig> {


}
