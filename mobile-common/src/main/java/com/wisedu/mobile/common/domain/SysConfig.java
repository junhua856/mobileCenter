package com.wisedu.mobile.common.domain;

import com.wisedu.mobile.common.BaseEntity;
import org.nutz.dao.entity.annotation.*;
import org.nutz.plugins.validation.annotation.Validations;

import java.io.Serializable;


/**
 * 系统参数表 sys_config
 *
 * @author hj
 * @date 2019-04-17
 */
@Table("sys_config")
@Comment("配置表")
public class SysConfig extends BaseEntity implements Serializable {
    private static final long serialVersionUID = 1L;

    @Column("config_key")
    @Comment("配置项")
    @ColDefine(type = ColType.VARCHAR, width = 200)
    @Validations(required=true,errorMsg = "配置项不能为空")
    private String configKey;

    /**
     * 参数值
     */
    @Column("config_val")
    @Comment("参数值")
    @ColDefine(type = ColType.VARCHAR, width = 500)
    @Validations(required=true,errorMsg = "参数值不能为空")
    private String configVal;

    /**
     * 说明
     */
    @Column("note")
    @Comment("说明")
    private String note;



    public String getConfigKey() {
        return configKey;
    }

    public void setConfigKey(String configKey) {
        this.configKey = configKey;
    }

    public String getConfigVal() {
        return configVal;
    }

    public void setConfigVal(String configVal) {
        this.configVal = configVal;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

}
