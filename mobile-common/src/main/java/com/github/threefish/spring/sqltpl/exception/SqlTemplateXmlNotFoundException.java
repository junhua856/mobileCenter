package com.github.threefish.spring.sqltpl.exception;

/**
 * @author 黄川 huchuc@vip.qq.com
 * date: 2020/3/11
 */
public class SqlTemplateXmlNotFoundException extends RuntimeException {

    public SqlTemplateXmlNotFoundException(String message) {
        super(message);
    }

    public SqlTemplateXmlNotFoundException(Throwable cause) {
        super(cause);
    }
}
