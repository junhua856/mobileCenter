INSERT INTO sys_menu VALUES ('35cb950cebb04bb18bb1d8b742a02xyz', '获取用户信息', '6ca1d8cbc01543578a83443564d4a32c', '1', '/auth/user', 'POST', '2', '0', null, null, null, null, null, null, null, null, null);
INSERT INTO sys_menu VALUES ('35cb950cebb04bb18bb1d8b742a02xcc', '菜单管理', '35cb950cebb04bb18bb1d8b742a02xxx', '1', '/manage/sys/menu/index', 'GET', '1', '0', null, null, null, null, null, null, null, null, null);
INSERT INTO sys_menu VALUES ('35cb950cebb04bb18bb1d8b742a02xxx', '系统管理', null, '1', null, null, '0', '0', null, null, null, null, null, null, null, null, null);
INSERT INTO sys_menu VALUES ('35cb950cebb04bb18bb1d8b742a02xzz', '用户管理', '35cb950cebb04bb18bb1d8b742a02xxx', '1', '/manage/sys/user/index', 'GET', '1', '0', null, null, null, null, null, null, null, null, null);
INSERT INTO sys_menu VALUES ('6ca1d8cbc01543578a83443564d4a32c', '接口', null, '2', null, null, '0', '0', null, null, null, null, null, null, null, null, null);
INSERT INTO sys_menu VALUES ('914aa22c78af4327822061f3eada4067', '实时日志', '35cb950cebb04bb18bb1d8b742a02xxx', '1', '/manage/logging', 'GET', '1', '0', null, null, null, null, null, null, null, null, null);
INSERT INTO sys_menu VALUES ('9a82ea3840794382b4ff14ddfccf9ff0', '角色管理', '35cb950cebb04bb18bb1d8b742a02xxx', '2', '/manage/sys/role/index', 'GET', '1', '0', null, null, null, null, null, null, null, null, null);
INSERT INTO sys_menu VALUES ('bcf17dc0ce304f0ba02d64ce21ddb4f9', '组织管理', '35cb950cebb04bb18bb1d8b742a02xxx', '1', '/manage/sys/dept/index', 'GET', '1', '0', null, null, null, null, null, null, null, null, null);
INSERT INTO sys_menu VALUES ('35cb950cebb04bb18bb1d8b742a02xzz', '字典管理', '35cb950cebb04bb18bb1d8b742a02xxx', '2', '/manage/sys/dict/index', 'GET', '1', '0', null, null, null, null, null, null, null, null, null);
INSERT INTO sys_menu VALUES ('5a939c9149d945c096bdef680235b793', '配置管理', '35cb950cebb04bb18bb1d8b742a02xxx', '1', '/manage/sys/config/index', 'GET', '1', '0', null, null, null, null, null, null, null, null, null);


INSERT INTO sys_role VALUES ('r0001', 'admin', 'admin', '0', '0', null, null, null, null);

INSERT INTO sys_role_menu VALUES ('r0001', '35cb950cebb04bb18bb1d8b742a02xcc');
INSERT INTO sys_role_menu VALUES ('r0001', '35cb950cebb04bb18bb1d8b742a02xxx');
INSERT INTO sys_role_menu VALUES ('r0001', '35cb950cebb04bb18bb1d8b742a02xzz');
INSERT INTO sys_role_menu VALUES ('r0001', '914aa22c78af4327822061f3eada4067');
INSERT INTO sys_role_menu VALUES ('r0001', 'bcf17dc0ce304f0ba02d64ce21ddb4f9');
INSERT INTO sys_role_menu VALUES ('r0001', '35cb950cebb04bb18bb1d8b742a02005');
INSERT INTO sys_role_menu VALUES ('r0001', '9a82ea3840794382b4ff14ddfccf9ff0');
INSERT INTO sys_role_menu VALUES ('r0001', '35cb950cebb04bb18bb1d8b742a02xyz');
INSERT INTO sys_role_menu VALUES ('r0001', '5a939c9149d945c096bdef680235b793');
INSERT INTO sys_role_menu VALUES ('r0001', '35cb950cebb04bb18bb1d8b742a02xzz');

INSERT INTO sys_user VALUES ('1', null, 'hj', 'hujun','$2a$10$uVqTAHkdWWbFdUKwYiIhmuN2Vhvr2hzXa9U0BzkaVG55owGDpxmIu', '','137', '男', '', 'blue', 'en_US', '0', null, null, null, null);

INSERT INTO sys_user_role VALUES ('1', 'r0001');