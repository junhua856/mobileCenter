package com.wisedu.mobile.auth.service.impl.v1.db;

import com.github.threefish.spring.sqltpl.annotation.SqlXml;
import com.wisedu.mobile.common.BaseServiceImpl;
import com.wisedu.mobile.common.domain.SysDept;
import com.wisedu.mobile.common.service.ISysDeptService;
import org.nutz.dao.Cnd;
import org.nutz.dao.entity.Record;
import org.nutz.dao.util.cri.Exps;
import org.nutz.lang.Lang;
import org.nutz.lang.Strings;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @Author: hj
 * @Date: 2020/3/17 上午11:01
 */
@Service
public class SysDeptServiceImpl extends BaseServiceImpl<SysDept> implements ISysDeptService {

    /**
     * 构建前端所需要下拉树结构
     *
     * @param depts 组织列表
     * @return 下拉树结构列表
     */
    public List<SysDept> buildDeptTree(List<SysDept> depts) {
        Map<String,SysDept> sysDepts = new HashMap<>();
        for(SysDept dept:depts){
            if(Strings.isEmpty(dept.getParentId())){
                if(sysDepts.get(dept.getId())!=null){
                    dept.setChildren(sysDepts.get(dept.getId()).getChildren());
                }
                sysDepts.put(dept.getId(),dept);
            }else{
                if(sysDepts.get(dept.getParentId())!=null){
                    sysDepts.get(dept.getParentId()).getChildren().add(dept);
                }else {
                    SysDept m = new SysDept();
                    m.getChildren().add(dept);
                    sysDepts.put(dept.getParentId(),m);
                }
            }
        }
        return Lang.collection2list(sysDepts.values());
    }
}
