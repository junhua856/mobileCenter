package com.wisedu.mobile.manage.controller;

import com.wisedu.mobile.common.AjaxResult;
import com.wisedu.mobile.common.redis.RedisCache;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

/**
 * @Author: hj
 * @Date: 2020/3/17 下午1:50
 */
@Controller
@RequestMapping("/manage/cache")
public class CacheController{

    @Autowired
    private RedisCache redisCache;

    /**
     * 缓存管理页面
     */
    @GetMapping("")
    public String cache() {
        return "cache";
    }

    @DeleteMapping("/delete/{id}")
    @ResponseBody
    public AjaxResult delete(@PathVariable("id") String id) {
        System.out.println("========1=========");
        redisCache.deleteObjectByKeyPre(id);
        return AjaxResult.success();
    }
}
