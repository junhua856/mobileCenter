import org.nutz.lang.Lang;

import java.util.HashMap;
import java.util.Map;

/**
 * @Author: hj
 * @Date: 2020/3/26 上午10:23
 */
public class MapTest {
    public static void main(String[] args) {
        Map<String,String> m = new HashMap();
        for(int i = 1;i<10;i++) {
            m.put(""+i,""+i);
        }
        for(Map.Entry<String,String> entry:m.entrySet()){
            System.out.println(entry.getKey());
        }

    }
}
