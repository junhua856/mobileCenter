## 简介 
MobileCenter 一套简单通用的接口平台系统 
分为5大模块：管理中心、业务中心、认证中心、消息中心、核心模块 

## 技术栈 
前端：jquery layui 
java后端：SpringBoot + Thymeleaf + WebSocket + Spring Security + druid + nutz + redis + jwt + restful + swagger 

## 初始化sql文件命名规则
V1__XXX.sql
首位大写字母V ，固定格式；
后面跟上版本号，版本号为数字、点、或者下划线组成；
版本号后跟上__(注意：这里是两位下划线)，固定格式；
__后跟上文件描述，无限制，最好做到见文知意；
如：V20180717__creatTableStudent.sql

## 配置规则
各模块提供各自独立的 .properties

## 国际化规则
各模块提供各自独立的 i18n
国际化可设置到单个用户

## orm
beetl引擎xml，支持参数、变量
各类方法参见BaseServiceImpl

## mvc
继承BaseController会有意想不到的效果

## 限流
基于redis，时间窗口算法

## 权限
RBAC

## 日志
业务日志
登陆登出日志
接口日志

## 接口文档
http://ip:port/doc.html