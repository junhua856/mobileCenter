package com.wisedu.mobile.core.config;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@ConfigurationProperties(prefix = "rsa.encrypt")
@Configuration
public class SecretKeyConfig {

    private String H5Key;

    private String wxKey;

    private String appKey;

    private String charset = "UTF-8";

    private boolean open = true;

    private boolean showLog = false;

    public String getH5Key() {
        return H5Key;
    }

    public void setH5Key(String h5Key) {
        H5Key = h5Key;
    }

    public String getWxKey() {
        return wxKey;
    }

    public void setWxKey(String wxKey) {
        this.wxKey = wxKey;
    }

    public String getAppKey() {
        return appKey;
    }

    public void setAppKey(String appKey) {
        this.appKey = appKey;
    }

    public String getCharset() {
        return charset;
    }

    public void setCharset(String charset) {
        this.charset = charset;
    }

    public boolean isOpen() {
        return open;
    }

    public void setOpen(boolean open) {
        this.open = open;
    }

    public boolean isShowLog() {
        return showLog;
    }

    public void setShowLog(boolean showLog) {
        this.showLog = showLog;
    }

    public String getKey(String appId) {
        if("H5".equals(appId)){
            return  this.H5Key;
        }else if("WX".equals(appId)){
            return this.wxKey;
        }else if("APP".equals(appId)){
            return this.appKey;
        }else{
            return this.H5Key;
        }
    }

}
