package com.wisedu.mobile.common.constant;

/**
 * redis常量信息
 * 
 * @author hj
 */
public class RedisConstants
{
    /**
     * 2k标志
     */
    public static final String K_MENU_KEY = "2kMenu";

    /** 限流标志 */
    public static final String LIMIT_MENU_KEY = "LimiterMenu";

}
