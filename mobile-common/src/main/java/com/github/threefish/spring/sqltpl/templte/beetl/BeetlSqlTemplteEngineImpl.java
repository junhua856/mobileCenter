package com.github.threefish.spring.sqltpl.templte.beetl;

import com.github.threefish.spring.sqltpl.service.ISqlTemplteEngine;
import org.beetl.core.Configuration;
import org.beetl.core.GroupTemplate;
import org.beetl.core.Template;
import org.beetl.core.exception.BeetlException;
import org.beetl.core.resource.StringTemplateResourceLoader;

import java.io.IOException;
import java.util.Map;

/**
 * @author 黄川 huchuc@vip.qq.com
 * <p>Date: 2019/2/19</p>
 */
public class BeetlSqlTemplteEngineImpl implements ISqlTemplteEngine {

    GroupTemplate gt;
    String statementStart = "<exp>";
    String statementEnd = "</exp>";

    public BeetlSqlTemplteEngineImpl(GroupTemplate gt) {
        this.gt = gt;
    }

    public BeetlSqlTemplteEngineImpl() {
        try {
            Configuration cfg = Configuration.defaultConfiguration();
            cfg.setStatementStart(statementStart);
            cfg.setStatementEnd(statementEnd);
            cfg.setHtmlTagSupport(false);
            this.gt = new GroupTemplate(new StringTemplateResourceLoader(), cfg);
            this.gt.setErrorHandler((beeExceptionos, writer) -> {
                throw beeExceptionos;
            });
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void setStatementStart(String statementStart) {
        this.statementStart = statementStart;
    }

    public void setStatementEnd(String statementEnd) {
        this.statementEnd = statementEnd;
    }

    /**
     * 解析模版
     *
     * @param templeText 模版内容
     * @param bindData   绑定参数
     * @return 渲染后的SQL
     */
    @Override
    public String render(String templeText, Map bindData) throws BeetlException {
        Template template = gt.getTemplate(templeText);
        template.binding(bindData);
        return template.render().trim();
    }

    /**
     * 取得后可以注册很多方法或函数等等
     *
     * @return GroupTemplate
     */
    public GroupTemplate getGt() {
        return this.gt;
    }
}
