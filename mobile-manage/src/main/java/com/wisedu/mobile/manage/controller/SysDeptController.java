package com.wisedu.mobile.manage.controller;

import com.wisedu.mobile.auth.service.impl.v1.db.SysDeptServiceImpl;
import com.wisedu.mobile.auth.service.impl.v1.db.SysMenuServiceImpl;
import com.wisedu.mobile.common.AjaxResult;
import com.wisedu.mobile.common.BaseController;
import com.wisedu.mobile.common.domain.SysDept;
import com.wisedu.mobile.common.domain.SysMenu;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * @Author: hj
 * @Date: 2020/3/24 下午4:28
 */
@RestController
@RequestMapping("/manage/sys/dept")
public class SysDeptController extends BaseController<SysDept> {

    @Autowired
    SysDeptServiceImpl sysDeptService;

    @PostMapping("tree")
    public AjaxResult<List<SysDept>> tree() {
        List<SysDept> depts = sysDeptService.query();
        return AjaxResult.success(sysDeptService.buildDeptTree(depts));
    }
}
