package com.wisedu.mobile.common.domain;

import com.wisedu.mobile.common.BaseEntity;
import org.apache.logging.log4j.util.Strings;
import org.nutz.dao.entity.annotation.*;
import org.nutz.plugins.validation.annotation.Validations;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

/**
 * 菜单权限表 sys_menu
 * 
 * @author hj
 */
@Table("sys_menu")
@Comment("菜单表")
public class SysMenu extends BaseEntity
{
    private static final long serialVersionUID = 1L;


    @Column("menu_name")
    @Comment("菜单名称")
    @ColDefine(type = ColType.VARCHAR, width = 100)
    @Validations(required=true,errorMsg = "菜单名称不能为空")
    private String menuName;

    @Column("parent_id")
    @Comment("父菜单ID")
    @ColDefine(type = ColType.VARCHAR, width = 64)
    private String parentId;

    /**
     * 父菜单名称
     */
    private String parentName;

    @Column("order_num")
    @Comment("显示顺序")
    @Validations(required=true,errorMsg = "排序不能为空")
    @ColDefine(type = ColType.VARCHAR, width = 10)
    private String orderNum;

    @Column("url")
    @Comment("菜单URL")
    @ColDefine(type = ColType.VARCHAR, width = 100)
    private String url;

    @Column
    @Comment("地址httpmethod")
    @ColDefine(type = ColType.VARCHAR, width = 100)
    private String httpMethod;

    @Column("menu_type")
    @Comment("类型:0目录,1菜单,2接口")
    @ColDefine(type = ColType.VARCHAR, width = 10)
    @Validations(required=true,errorMsg = "菜单类型不能为空")
    private String menuType;

    @Column("status")
    @Comment("菜单状态:0显示,1隐藏")
    @ColDefine(type = ColType.VARCHAR, width = 10)
    private int status;

    @Column
    @Comment("菜单图标")
    @ColDefine(type = ColType.VARCHAR, width = 100)
    private String icon;

    @Column("base_exp")
    @Comment("限流标准")
    @ColDefine(type = ColType.VARCHAR, width = 100)
    private String baseExp;
    @Column("time_unit")
    @Comment("限流时间窗口")
    @ColDefine(type = ColType.VARCHAR, width = 50)
    private String timeUnit;
    @Column
    @Comment("限流次数")
    @ColDefine(type = ColType.VARCHAR, width = 10)
    private String permits;
    @Column("enhance_url")
    @Comment("2kURL")
    @ColDefine(type = ColType.VARCHAR, width = 100)
    private String enhanceUrl;

    /** 子菜单 */
    private List<SysMenu> children = new ArrayList<SysMenu>();

    private boolean checked;

    public boolean isChecked() {
        return checked;
    }

    public void setChecked(boolean checked) {
        this.checked = checked;
    }

    public String getMenuName() {
        return menuName;
    }

    public void setMenuName(String menuName) {
        this.menuName = menuName;
    }

    public String getParentId() {
        return parentId;
    }

    public void setParentId(String parentId) {
        this.parentId = parentId;
    }

    public String getParentName() {
        return parentName;
    }

    public void setParentName(String parentName) {
        this.parentName = parentName;
    }

    public String getOrderNum() {
        return orderNum;
    }

    public void setOrderNum(String orderNum) {
        this.orderNum = orderNum;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getMenuType() {
        return menuType;
    }

    public void setMenuType(String menuType) {
        this.menuType = menuType;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }


    public List<SysMenu> getChildren() {
        return children;
    }

    public void setChildren(List<SysMenu> children) {
        this.children = children;
    }

    public String getBaseExp() {
        return baseExp;
    }

    public void setBaseExp(String baseExp) {
        this.baseExp = baseExp;
    }

    public String getTimeUnit() {
        return timeUnit;
    }

    public void setTimeUnit(String timeUnit) {
        this.timeUnit = timeUnit;
    }

    public String getPermits() {
        return permits;
    }

    public void setPermits(String permits) {
        this.permits = permits;
    }

    public String getHttpMethod() {
        return httpMethod;
    }

    public void setHttpMethod(String httpMethod) {
        this.httpMethod = httpMethod;
    }

    public String getEnhanceUrl() {
        return enhanceUrl;
    }

    public void setEnhanceUrl(String enhanceUrl) {
        this.enhanceUrl = enhanceUrl;
    }
}
