package com.wisedu.mobile.common.service;

import com.wisedu.mobile.common.constant.Constants;
import com.wisedu.mobile.common.domain.LoginUser;
import com.wisedu.mobile.common.redis.RedisCache;
import com.wisedu.mobile.common.utils.MessageUtils;
import com.wisedu.mobile.common.exception.CaptchaException;
import com.wisedu.mobile.common.exception.CaptchaExpireException;
import com.wisedu.mobile.common.exception.CustomException;
import com.wisedu.mobile.common.exception.UserPasswordNotMatchException;
import com.wisedu.mobile.common.utils.ServletUtils;
import eu.bitwalker.useragentutils.UserAgent;
import org.nutz.lang.Lang;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;

/**
 * 登录校验方法
 * 
 * @author hj
 */
@Component
public class SysLoginService
{
    @Autowired
    private TokenService tokenService;

    @Resource
    private AuthenticationManager authenticationManager;

    @Autowired
    private RedisCache redisCache;

    @Autowired
    LogService logService;

    /**
     * 登录验证
     * 
     * @param loginname 用户名
     * @param password 密码
     * @param code 验证码
     * @param uuid 唯一标识
     * @return 结果
     */
    public String loginWithCode(String loginname, String password, String code, String uuid)
    {
        String verifyKey = Constants.CAPTCHA_CODE_KEY + uuid;
        String captcha = redisCache.getCacheObject(verifyKey);
        redisCache.deleteObject(verifyKey);
        final UserAgent userAgent = UserAgent.parseUserAgentString(ServletUtils.getRequest().getHeader("User-Agent"));
        final String ip = Lang.getIP(ServletUtils.getRequest());
        // 获取客户端操作系统
        String os = userAgent.getOperatingSystem().getName();
        // 获取客户端浏览器
        String browser = userAgent.getBrowser().getName();
        if (captcha == null)
        {
            logService.recordLogininfor(loginname, Constants.LOGIN_FAIL, MessageUtils.message("user.jcaptcha.expire"),ip,os,browser);
            throw new CaptchaExpireException();
        }
        if (!code.equalsIgnoreCase(captcha))
        {
            logService.recordLogininfor(loginname, Constants.LOGIN_FAIL, MessageUtils.message("user.jcaptcha.error"),ip,os,browser);
            throw new CaptchaException();
        }
        return login( loginname,  password);
    }

    /**
     * 登录验证
     *
     * @param loginname 用户名
     * @param password 密码
     * @return 结果
     */
    public String login(String loginname, String password)
    {
        final UserAgent userAgent = UserAgent.parseUserAgentString(ServletUtils.getRequest().getHeader("User-Agent"));
        final String ip = Lang.getIP(ServletUtils.getRequest());
        // 获取客户端操作系统
        String os = userAgent.getOperatingSystem().getName();
        // 获取客户端浏览器
        String browser = userAgent.getBrowser().getName();
        // 用户验证
        Authentication authentication = null;
        try
        {
            // 该方法会去调用UserDetailsServiceImpl.loadUserByUsername
            authentication = authenticationManager
                    .authenticate(new UsernamePasswordAuthenticationToken(loginname, password));
        }
        catch (Exception e)
        {
            if (e instanceof BadCredentialsException)
            {
                logService.recordLogininfor(loginname, Constants.LOGIN_FAIL, MessageUtils.message("user.password.not.match"),ip,os,browser);
                throw new UserPasswordNotMatchException();
            }
            else
            {
                logService.recordLogininfor(loginname, Constants.LOGIN_FAIL, e.getMessage(),ip,os,browser);
                throw new CustomException(e.getMessage());
            }
        }
        LoginUser loginUser = (LoginUser) authentication.getPrincipal();

        logService.recordLogininfor(loginname, Constants.LOGIN_SUCCESS, MessageUtils.message("user.login.success"),ip,os,browser);

        // 生成token
        return tokenService.createToken(loginUser);
    }
}
