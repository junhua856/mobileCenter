package com.wisedu.mobile.common.service.impl;

import com.wisedu.mobile.common.BaseServiceImpl;
import com.wisedu.mobile.common.domain.SysConfig;
import com.wisedu.mobile.common.service.ISysConfigService;
import org.springframework.stereotype.Service;

/**
 * @Author: hj
 * @Date: 2020/4/22 下午1:37
 */
@Service
public class SysConfigServiceImpl extends BaseServiceImpl<SysConfig> implements ISysConfigService {
}
