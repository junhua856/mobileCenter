package com.wisedu.mobile.core.security;

import com.wisedu.mobile.common.AjaxResult;
import com.wisedu.mobile.common.constant.HttpStatus;
import com.wisedu.mobile.common.utils.MessageUtils;
import com.wisedu.mobile.common.utils.ServletUtils;
import org.nutz.json.Json;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.AuthenticationEntryPoint;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.Serializable;
import java.text.MessageFormat;

/**
 * 认证失败处理类 返回未授权
 * 
 * @author hj
 */
@Component
public class AuthenticationEntryPointImpl implements AuthenticationEntryPoint, Serializable
{
    private static final long serialVersionUID = -8970718410437077606L;

    @Override
    public void commence(HttpServletRequest request, HttpServletResponse response, AuthenticationException e)
            throws IOException
    {
        int code = HttpStatus.UNAUTHORIZED;
        String msg = MessageFormat.format(MessageUtils.message("no.auth.permission"), request.getRequestURI());
        ServletUtils.renderString(response, Json.toJson(AjaxResult.error(code, msg)));
    }
}
