package com.wisedu.mobile;

import com.github.threefish.spring.sqltpl.annotation.EnableSqlTpl;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.scheduling.annotation.EnableAsync;

/**
 * 启动程序
 * 
 * @author hj
 */
@SpringBootApplication(exclude = { DataSourceAutoConfiguration.class })
@EnableSqlTpl
@EnableAsync
@EnableCaching
public class MobileApplication
{
    public static void main(String[] args)
    {
        // System.setProperty("spring.devtools.restart.enabled", "false");
        SpringApplication.run(MobileApplication.class, args);
        System.out.println("================启动成功============");
    }

}